import { Station } from "./entities/Station";
import { ObjectType, Field } from "type-graphql";
import { BasicStation } from "./entities/BasicStation";
import { Substation } from "./entities/Substation";

@ObjectType()
export class StationReadings {
  public station!: BasicStation|Substation;
}
