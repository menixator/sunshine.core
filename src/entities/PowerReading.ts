import { Field, Float, ID, ObjectType } from "type-graphql";
import {
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  Column,
  Index
} from "typeorm";
import { Station } from "./Station";
import { BaseReading } from "./BaseReading";

@Entity()
@ObjectType()
@Index(
  "UQ_POWER_READING_IDX",
  reading => [reading.stationId, reading.timestamp],
  {
    unique: true
  }
)
export class PowerReading extends BaseReading {
  @Column("int")
  stationId!: number;

  @ManyToOne(() => Station, station => station.powerReadings)
  station!: Station;
}
