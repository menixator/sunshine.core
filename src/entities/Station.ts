import { Field, ID, ObjectType, InterfaceType } from "type-graphql";
import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  TableInheritance,
  OneToMany
} from "typeorm";
import { StationType } from "../types/StationType";
import { PowerReading } from "./PowerReading";
import { EnergyReading } from "./EnergyReading";
import { StationReadings } from "../StationReadings";

@Entity("station")
@TableInheritance({ column: { type: "int", name: "type" } })
@InterfaceType()
export class Station {
  @Field(type => ID, {
    description: "Station identifier"
  })
  @PrimaryGeneratedColumn()
  id!: number;

  @Field(() => StationType)
  @Column("int")
  type!: StationType;

  @Index({
    unique: true
  })
  @Field({ description: "The name of the station" })
  @Column({
    type: "varchar",
    length: 64,
    nullable: false
  })
  name!: string;

  @Field(() => ID, {
    description: "UUID of the station"
  })
  @Column({
    type: "varchar",
    length: 36,
    nullable: false
  })
  uuid!: string;

  @Field({
    nullable: true,
    description: "The date that the Station was installed"
  })
  @Column({
    nullable: true
  })
  installedDate!: Date;

  @Field({
    nullable: true,
    description: "The location of the Station"
  })
  @Column({
    nullable: true
  })
  location!: string;

  @Field({
    nullable: false,
    description: "Readings interface"
  })
  private readings!: StationReadings;

  @OneToMany(() => PowerReading, reading => reading.station)
  powerReadings!: PowerReading[];

  @OneToMany(() => EnergyReading, reading => reading.station)
  energyReadings!: EnergyReading[];
}
