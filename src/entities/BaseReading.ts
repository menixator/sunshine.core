import { Field, Float, ID, ObjectType } from "type-graphql";
import {
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  Column,
  Index
} from "typeorm";
import { Station } from "./Station";
import { CalculatedReading } from "../types/CalculatedReading";

@ObjectType()
export class BaseReading extends CalculatedReading {
  @Field({ description: "When the row was created" })
  @CreateDateColumn()
  createdDate!: Date;

  @Field({
    description: " When the row was last update"
  })
  @UpdateDateColumn()
  updatedDate!: Date;

  @Field()
  @Column()
  timestamp!: Date;
}
