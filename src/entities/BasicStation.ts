import { ObjectType } from "type-graphql";
import { ChildEntity, Entity, Column } from "typeorm";
import { StationType } from "../types/StationType";
import { Station } from "./Station";

@ObjectType({
  implements: Station
})
@ChildEntity(StationType.BASICSTATION)
export class BasicStation extends Station {
  type = StationType.BASICSTATION
}
