import { Field, ObjectType } from "type-graphql";
import { Column, Entity, Index, ManyToOne } from "typeorm";
import { BaseReading } from "./BaseReading";
import { Station } from "./Station";

@Entity()
@ObjectType()
@Index(
  "UQ_ENERGY_READINGS",
  reading => [reading.stationId, reading.timestamp],
  {
    unique: true
  }
)
export class EnergyReading extends BaseReading {
  @Column("int")
  stationId!: number;
  
  @Field(() => Station)
  @ManyToOne(() => Station, station => station.energyReadings)
  station!: Station;
}
