import { ObjectType, Field } from "type-graphql";
import { ManyToOne, ChildEntity } from "typeorm";
import { StationType } from "../types/StationType";
import { StationGroup } from "./StationGroup";
import { Station } from "./Station";

@ObjectType({
  implements: Station
})
@ChildEntity(StationType.SUBSTATION)
export class Substation extends Station {
  type = StationType.SUBSTATION;

  @Field(() => StationGroup, {
    description: "The StationGroup that the Substation belongs to"
  })
  @ManyToOne(type => StationGroup, {
    cascade: ["remove", "insert", "update"],
    nullable: true
  })
  group!: StationGroup;
}
