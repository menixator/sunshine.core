import { Field, ID, ObjectType } from "type-graphql";
import { Column, Index, OneToMany, PrimaryGeneratedColumn, Entity } from "typeorm";
import { Substation } from "./Substation";

@Entity()
@ObjectType()
export class StationGroup  {
  @Field(type => ID, {
    description: "Station identifier"
  })
  @PrimaryGeneratedColumn()
  id!: number;

  @Index({
    unique: true
  })
  @Field({ description: "The name of the Station Group" })
  @Column({
    type: "varchar",
    length: 64,
    nullable: false
  })
  name!: string;
  @Field(() => [Substation])
  @OneToMany(type => Substation, station => station.group)
  substations!: Substation[];
}
