import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from "typeorm";
import { User } from "./User";
import { ObjectType, Field } from "type-graphql";
import uuid from "uuid/v4";

export const LAST_TOUCHED_EXPIRATION = 1 * 24 * 60 * 60 * 1000;
export const COOKIE_NAME = "session";

@Entity("auth_tokens")
@ObjectType({
  description: "A token used to keep a user logged into the application"
})
export class AuthToken {
  static generateToken() {
    return uuid();
  }

  @Field({
    description: "Authentication token identifier"
  })
  @PrimaryGeneratedColumn()
  id!: number;

  @Field({
    description: "The identifier iteself"
  })
  @Column({
    nullable: false
  })
  value!: string;

  @Field()
  @CreateDateColumn()
  created!: Date;

  @Field()
  @Column({
    type: "datetime",
    default: () => "CURRENT_TIMESTAMP",
    nullable: false
  })
  lastTouched!: Date;

  get expired() {
    return Date.now() - this.lastTouched.getTime() > LAST_TOUCHED_EXPIRATION;
  }

  public touch() {
    this.lastTouched = new Date();
  }

  @ManyToOne(type => User, {
    onDelete: "CASCADE",
    nullable: false
  })
  @JoinColumn()
  user!: User;
}
