import { registerEnumType } from "type-graphql";

export enum PowerUnit {
  KW,
  W
}

registerEnumType(PowerUnit, {
  name: "PowerUnit",
  description: "Units for Power Readings"
});
