export enum CalculatedReadingTypes {
  E_MONTHLY = "EnergyMonthly",
  E_YEARLY = "EnergyYearly",
  E_DAILY = "EnergyDaily",

  P_GROUPED = "PowerGrouped",

  E_GROUPED_MONTHLY = "EnergyGroupedMonthly",
  E_GROUPED_YEARLY = "EnergyGroupedYearly",
  E_GROUPED_DAILY = "EnergyGroupedDaily",

  R_GROUPED_DAILY = "ReimbursementGroupedDaily",
  R_GROUPED_MONTHLY = "ReimbursementGroupedMonthly",
  R_GROUPED_YEARLY = "ReimbursementGroupedYearly",

  R_DAILY = "ReimbursementDaily",
  R_MONTHLY = "ReimbursementMonthly",
  R_YEARLY = "ReimbursementYearly"
}
