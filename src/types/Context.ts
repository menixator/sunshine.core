import { Request, Response } from "express";
import { AuthToken } from "../entities/AuthToken";
import { User } from "../entities/User";
import { Role } from "../entities/Role";
import { BasicStation } from "../entities/BasicStation";
import { Substation } from "../entities/Substation";

export type RootContext = {
  req: Request;
  res: Response;
  token: AuthToken;
  user: User;
  role: Role;
  authorized: boolean;
  station?: BasicStation | Substation
};
