import { registerEnumType } from "type-graphql";

export enum StationType {
  /**
   * Start counting from
   */
  BASICSTATION = 1,
  SUBSTATION
}

registerEnumType(StationType, {
  description: "Type identifier for stations",
  name: "StationType"
});
