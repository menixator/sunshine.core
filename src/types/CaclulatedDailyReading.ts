import { Field, ObjectType, Int } from "type-graphql";
import { Month } from "./Month";
import { CalculatedMonthlyReading } from "./CalculatedMonthlyReading";

@ObjectType()
export class CalculatedDailyReading extends CalculatedMonthlyReading {
  @Field(() => Int)
  day!: number;
}
