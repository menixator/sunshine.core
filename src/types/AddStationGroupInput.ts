import { Field, InputType } from "type-graphql";
import { StationGroup } from "../entities/StationGroup";
@InputType()
export class AddStationGroupInput implements Partial<StationGroup> {
  @Field({ description: "The name of the StationGroup" })
  name!: string;
}
