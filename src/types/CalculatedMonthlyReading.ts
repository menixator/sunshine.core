import { Field, Int, ObjectType } from "type-graphql";
import { CaclulatedYearlyReading } from "./CalculatedYearlyReading";
import { Month } from "./Month";

@ObjectType()
export class CalculatedMonthlyReading extends CaclulatedYearlyReading {
  @Field(() => Month)
  month!: Month;
}
