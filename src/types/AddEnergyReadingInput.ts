import { Field, Float, InputType } from "type-graphql";
import { EnergyUnit } from "./EnergyUnit";

@InputType()
export class AddEnergyReading {
  @Field(() => Float, {
    description: "The reading value"
  })
  value!: number;

  @Field(() => EnergyUnit, {
    description: "The unit of the reading"
  })
  unit!: EnergyUnit;

  @Field({
    description: "The timestamp for the reading"
  })
  timestamp!: Date;
}
