import { Field, ID, Float, ObjectType } from "type-graphql";
import { PrimaryGeneratedColumn, Column } from "typeorm";

@ObjectType()
export class CalculatedReading {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id!: number;

  @Field(() => Float, {
    description: "The reading value"
  })
  @Column("float")
  value!: number;
}
