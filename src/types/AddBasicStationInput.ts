import { Field, ID, InputType } from "type-graphql";
import { Station } from "../entities/Station";
@InputType()
export class AddBasicStationInput implements Partial<Station> {
  @Field({ description: "The name of the station" })
  name!: string;
  @Field(() => ID, {
    description: "UUID of the station. Should be unique."
  })
  uuid!: string;
  @Field({
    nullable: true,
    description: "The date that the Station was installed"
  })
  installedDate?: Date;
  @Field({
    description: "The location of the Station",
    nullable: true
  })
  location?: string;
}
