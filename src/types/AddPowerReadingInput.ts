import { Field, Float, ID, InputType } from "type-graphql";
import { PowerUnit } from "./PowerUnit";

@InputType()
export class AddPowerReadingInput {
  @Field(() => Float, {
    description: "The reading value"
  })
  value!: number;

  @Field(() => PowerUnit, {
    description: "The unit of the reading"
  })
  unit!: PowerUnit;

  @Field({
    description: "The timestamp for the reading"
  })
  timestamp!: Date;
}
