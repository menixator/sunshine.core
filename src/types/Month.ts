import { registerEnumType } from "type-graphql";

export enum Month {
  JAN,
  FEB,
  MAR,
  APR,
  MAY,
  JUN,
  JUL,
  AUG,
  SEP,
  OCT,
  NOV,
  DEC
}

registerEnumType(Month, {
  name: "Month",
  description: "Enumerator containing all months of the calender"
});
