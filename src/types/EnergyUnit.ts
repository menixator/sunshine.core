import { registerEnumType } from "type-graphql";

export enum EnergyUnit {
  KWH,
  WH
}

registerEnumType(EnergyUnit, {
  name: "EnergyUnit",
  description: "Units for Energy Readings"
});
