import { Field, ObjectType, Int } from "type-graphql";
import { CalculatedMonthlyReading } from "./CalculatedMonthlyReading";
import { Month } from "./Month";
import { CalculatedReading } from "./CalculatedReading";

@ObjectType()
export class CaclulatedYearlyReading extends CalculatedReading {
  @Field(() => Int)
  year!: number;
}
