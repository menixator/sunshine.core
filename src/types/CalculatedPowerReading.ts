import { Field, ObjectType } from "type-graphql";
import { CalculatedReading } from "./CalculatedReading";

@ObjectType()
export class CalculatedPowerReading extends CalculatedReading {
  @Field()
  timestamp!: Date;
}
