import { registerEnumType } from "type-graphql";

export enum EnergyInterval {
  MONTHLY,
  DAILY,
  YEARLY
}

registerEnumType(EnergyInterval, {
  name: "EnergyInterval",
  description: "Intervals for retrieving Energy Data"
});
