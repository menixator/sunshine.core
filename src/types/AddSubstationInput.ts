import { Field, ID, InputType } from "type-graphql";
import { Substation } from "../entities/Substation";
import { AddBasicStationInput } from "./AddBasicStationInput";

@InputType()
export class AddSubstationInput extends AddBasicStationInput implements Partial<Substation> {
  @Field(() => ID, {
    description: "ID of the station group"
  })
  groupId!: number;
}
