import { Arg, FieldResolver, ID, Query, Resolver, Root } from "type-graphql";
import { EntityManager } from "typeorm";
import { InjectManager } from "typeorm-typedi-extensions";
import { BasicStation } from "../entities/BasicStation";
import { Station } from "../entities/Station";
import { Substation } from "../entities/Substation";
import { UniqueConstraintError } from "../errors/UniqueConstraintError";
import { StationReadings } from "../StationReadings";
import { AddBasicStationInput } from "../types/AddBasicStationInput";


export class StationResolver {
  @InjectManager() manager!: EntityManager;

  // Check if name has been taken already
  protected async isNameTaken(name: string) {
    // Multiple stations can't have the same name.
    let foundStation = await this.manager.findOne(BasicStation, {
      name
    });

    return !!foundStation;
  }

  // Check if name has been taken already
  protected async isUUIDTaken(uuid: string) {
    // Multiple stations can't have the same name.
    let foundBasicStation = await this.manager.findOne(BasicStation, {
      uuid
    });

    if (foundBasicStation) return true;

    let foundSubstation = await this.manager.findOne(Substation, {
      uuid
    });

    if (foundSubstation) return true;

    return false;
  }

  /**
   *
   * @param EntityType Any Entity type
   * @param input an input shape class that extends from AddStationInput
   */
  protected async addStation<
    T extends AddBasicStationInput,
    V extends Station,
    K extends { new (): V }
  >(EntityType: K, input: T): Promise<V> {
    if (await this.isNameTaken(input.name)) {
      throw new UniqueConstraintError(EntityType, "name", input.name);
    }

    if (await this.isUUIDTaken(input.uuid)) {
      throw new UniqueConstraintError(EntityType, "uuid", input.uuid);
    }

    let station = new EntityType();
    station.name = input.name;
    station.uuid = input.uuid;
    if (input.installedDate) station.installedDate = input.installedDate;
    if (input.location) station.location = input.location;
    return station;
  }

  @Query(() => Station, { nullable: true })
  public async station(@Arg("id", () => ID) id: string) {
    return this.manager.findOne(Station, id);
  }

  protected getReadings(station: Station) {
    let readings = new StationReadings();
    readings.station = station;
    return readings;
  }
}
