import {
  Arg,
  Mutation,
  Resolver,
  Query,
  ID,
  FieldResolver,
  Root
} from "type-graphql";
import { EntityManager } from "typeorm";
import { InjectManager } from "typeorm-typedi-extensions";
import { StationGroup } from "../entities/StationGroup";
import { UniqueConstraintError } from "../errors/UniqueConstraintError";
import { AddStationGroupInput } from "../types/AddStationGroupInput";
import { StationGroupReadings } from "../StationGroupReadings";
import { Substation } from "../entities/Substation";

@Resolver(() => StationGroup)
export class StationGroupResolver {
  @InjectManager() manager!: EntityManager;

  @Mutation(() => StationGroup)
  async addStationGroup(@Arg("input") input: AddStationGroupInput) {
    let existingGroup = await this.manager.findOne(StationGroup, {
      name: input.name
    });

    if (existingGroup)
      throw new UniqueConstraintError(StationGroup, "name", input.name);

    let group = new StationGroup();
    group.name = input.name;
    await this.manager.insert(StationGroup, group);
    return group;
  }

  @Query(() => StationGroup, { nullable: true })
  async stationGroup(@Arg("id", () => ID) id: string) {
    return this.manager.findOne(StationGroup, id) || null;
  }

  @FieldResolver(() => StationGroupReadings)
  readings(@Root() stationGroup: StationGroup) {
    let stationGroupReadings = new StationGroupReadings();
    stationGroupReadings.stationGroup = stationGroup;
    return stationGroupReadings;
  }

  @FieldResolver(() => [Substation])
  async substations(@Root() stationGroup: StationGroup) {
    let substations = await this.manager
      .createQueryBuilder(StationGroup, "stationgroup")
      .relation("substations")
      .of(stationGroup)
      .loadMany();
      return substations;
  }
}
