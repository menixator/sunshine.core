import { Arg, FieldResolver, Resolver, Root } from "type-graphql";
import { EntityManager } from "typeorm";
import { InjectManager } from "typeorm-typedi-extensions";
import { EnergyReading } from "../entities/EnergyReading";
import { PowerReading } from "../entities/PowerReading";
import { StationGroupReadings } from "../StationGroupReadings";
import { CalculatedDailyReading } from "../types/CaclulatedDailyReading";
import { CalculatedMonthlyReading } from "../types/CalculatedMonthlyReading";
import { CalculatedPowerReading } from "../types/CalculatedPowerReading";
import { CalculatedReadingTypes } from "../types/CalculatedReadingTypes";
import { CaclulatedYearlyReading } from "../types/CalculatedYearlyReading";
import { StationType } from "../types/StationType";
import {
  dayFromTimestamp,
  monthFromTimestamp,
  paddedMonthFromTimestamp,
  sqlConcatWithSeperator,
  wrapString,
  yearFromTimestamp,
  toFixed
} from "../Utils";

@Resolver(() => StationGroupReadings)
export class StationGroupReadingsResolver {
  @InjectManager() manager!: EntityManager;

  @FieldResolver(() => [PowerReading])
  protected async power(
    @Root() { stationGroup }: StationGroupReadings,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    let qb = this.manager
      .createQueryBuilder(PowerReading, "powerReading")
      .innerJoin("powerReading.station", "station", "station.type = :type", {
        type: StationType.SUBSTATION
      })
      .innerJoin("station.group", "group")
      .where("powerReading.timestamp >= :from AND group.id = :groupId", {
        from,
        groupId: stationGroup.id
      });

    if (till) {
      qb.andWhere("powerReading.timestamp <= :till", { till });
    }
    qb.select(
      sqlConcatWithSeperator(
        `'/'`,
        wrapString(CalculatedReadingTypes.P_GROUPED),
        yearFromTimestamp(`powerReading.timestamp`),
        paddedMonthFromTimestamp(`powerReading.timestamp`),
        dayFromTimestamp(`powerReading.timestamp`)
      ),
      "id"
    )
      .addSelect(`SUM(powerReading.value)`, "value")
      .addSelect(`powerReading.timestamp`, "timestamp")
      .addGroupBy(`powerReading.timestamp`);

    return (await qb.getRawMany()).map(powerReadings => {
      let entity = new CalculatedPowerReading();
      Object.assign(entity, powerReadings);
      return entity;
    });
  }

  @FieldResolver(() => [CalculatedDailyReading])
  protected async energy(
    @Root() { stationGroup }: StationGroupReadings,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    let qb = this.manager
      .createQueryBuilder(EnergyReading, "energyReading")
      .innerJoin("energyReading.station", "station", "station.type = :type", {
        type: StationType.SUBSTATION
      })
      .innerJoin("station.group", "group")
      .where("energyReading.timestamp >= :from AND group.id = :groupId", {
        from,
        groupId: stationGroup.id
      });

    if (till) {
      qb.andWhere("energyReading.timestamp <= :till", { till });
    }

    qb.select(
      sqlConcatWithSeperator(
        `'/'`,
        wrapString(CalculatedReadingTypes.E_GROUPED_DAILY),
        yearFromTimestamp(`energyReading.timestamp`),
        paddedMonthFromTimestamp(`energyReading.timestamp`),
        dayFromTimestamp(`energyReading.timestamp`)
      ),
      "id"
    )
      .addSelect(`SUM(energyReading.value)`, "value")

      .addSelect(dayFromTimestamp(`energyReading.timestamp`), "day")
      .addSelect(monthFromTimestamp(`energyReading.timestamp`), "month")
      .addSelect(yearFromTimestamp(`energyReading.timestamp`), "year")

      .addGroupBy(dayFromTimestamp(`energyReading.timestamp`))
      .addGroupBy(monthFromTimestamp(`energyReading.timestamp`))
      .addGroupBy(yearFromTimestamp(`energyReading.timestamp`));

    return (await qb.getRawMany()).map(dailyEnergy => {
      let entity = new CalculatedDailyReading();
      Object.assign(entity, dailyEnergy);
      return entity;
    });
  }

  @FieldResolver(() => [CalculatedMonthlyReading])
  protected async energyByMonth(
    @Root() { stationGroup }: StationGroupReadings,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    let qb = this.manager
      .createQueryBuilder(EnergyReading, "energyReading")
      .innerJoin("energyReading.station", "station", "station.type = :type", {
        type: StationType.SUBSTATION
      })
      .innerJoin("station.group", "group")
      .where("energyReading.timestamp >= :from AND group.id = :groupId", {
        from,
        groupId: stationGroup.id
      })
      .select(
        sqlConcatWithSeperator(
          `'/'`,
          wrapString(CalculatedReadingTypes.E_GROUPED_MONTHLY),
          yearFromTimestamp(`energyReading.timestamp`),
          paddedMonthFromTimestamp(`energyReading.timestamp`)
        ),
        "id"
      )
      .addSelect(`SUM(energyReading.value)`, "value")
      .addSelect(monthFromTimestamp(`energyReading.timestamp`), "month")
      .addSelect(yearFromTimestamp(`energyReading.timestamp`), "year")

      .addGroupBy(monthFromTimestamp(`energyReading.timestamp`))
      .addGroupBy(yearFromTimestamp(`energyReading.timestamp`));

    if (till) {
      qb.andWhere("energyReading.timestamp <= :till", { till });
    }

    return (await qb.getRawMany()).map(monthlyEnergy => {
      let entity = new CalculatedMonthlyReading();
      Object.assign(entity, monthlyEnergy);
      return entity;
    });
  }

  @FieldResolver(() => [CaclulatedYearlyReading])
  protected async energyByYear(
    @Root() { stationGroup }: StationGroupReadings,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    let qb = this.manager
      .createQueryBuilder(EnergyReading, "energyReading")
      .innerJoin("energyReading.station", "station", "station.type = :type", {
        type: StationType.SUBSTATION
      })
      .innerJoin("station.group", "group")
      .where("energyReading.timestamp >= :from AND group.id = :groupId", {
        from,
        groupId: stationGroup.id
      })
      .select(
        sqlConcatWithSeperator(
          `'/'`,
          wrapString(CalculatedReadingTypes.E_GROUPED_YEARLY),
          yearFromTimestamp(`energyReading.timestamp`)
        ),
        "id"
      )
      .addSelect(`SUM(energyReading.value)`, "value")
      .addSelect(yearFromTimestamp(`energyReading.timestamp`), "year")
      .addGroupBy(yearFromTimestamp(`energyReading.timestamp`));

    if (till) {
      qb.andWhere("energyReading.timestamp <= :till", { till });
    }

    return (await qb.getRawMany()).map(yearlyEnergy => {
      let entity = new CaclulatedYearlyReading();
      Object.assign(entity, yearlyEnergy);
      return entity;
    });
  }

  @FieldResolver(() => [CalculatedDailyReading])
  protected async reimbursement(
    @Root() { stationGroup }: StationGroupReadings,
    @Arg("rate") rate: number,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    if (rate <= 0) {
      throw new RangeError(`Rate should be greater than 0`);
    }

    let qb = this.manager
      .createQueryBuilder(EnergyReading, "reimbursement")
      .innerJoin("reimbursement.station", "station", "station.type = :type", {
        type: StationType.SUBSTATION
      })
      .innerJoin("station.group", "group")
      .where("reimbursement.timestamp >= :from AND group.id = :groupId", {
        from,
        groupId: stationGroup.id
      });

    if (till) {
      qb.andWhere("reimbursement.timestamp <= :till", { till });
    }

    qb.select(
      sqlConcatWithSeperator(
        `'/'`,
        wrapString(CalculatedReadingTypes.R_GROUPED_DAILY),
        yearFromTimestamp(`reimbursement.timestamp`),
        paddedMonthFromTimestamp(`reimbursement.timestamp`),
        dayFromTimestamp(`reimbursement.timestamp`)
      ),
      "id"
    )
      .addSelect(toFixed(`SUM(reimbursement.value)*${rate}`), "value")

      .addSelect(dayFromTimestamp(`reimbursement.timestamp`), "day")
      .addSelect(monthFromTimestamp(`reimbursement.timestamp`), "month")
      .addSelect(yearFromTimestamp(`reimbursement.timestamp`), "year")

      .addGroupBy(dayFromTimestamp(`reimbursement.timestamp`))
      .addGroupBy(monthFromTimestamp(`reimbursement.timestamp`))
      .addGroupBy(yearFromTimestamp(`reimbursement.timestamp`));

    return (await qb.getRawMany()).map(dailyReimbursement => {
      let entity = new CalculatedDailyReading();
      Object.assign(entity, dailyReimbursement);
      return entity;
    });
  }

  @FieldResolver(() => [CalculatedMonthlyReading])
  protected async reimbursementByMonth(
    @Root() { stationGroup }: StationGroupReadings,
    @Arg("rate") rate: number,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    if (rate <= 0) {
      throw new RangeError(`Rate should be greater than 0`);
    }
    let qb = this.manager
      .createQueryBuilder(EnergyReading, "reimbursement")
      .innerJoin("reimbursement.station", "station", "station.type = :type", {
        type: StationType.SUBSTATION
      })
      .innerJoin("station.group", "group")
      .where("reimbursement.timestamp >= :from AND group.id = :groupId", {
        from,
        groupId: stationGroup.id
      })
      .select(
        sqlConcatWithSeperator(
          `'/'`,
          wrapString(CalculatedReadingTypes.R_GROUPED_MONTHLY),
          yearFromTimestamp(`reimbursement.timestamp`),
          paddedMonthFromTimestamp(`reimbursement.timestamp`)
        ),
        "id"
      )
      .addSelect(toFixed(`SUM(reimbursement.value)*${rate}`), "value")
      .addSelect(monthFromTimestamp(`reimbursement.timestamp`), "month")
      .addSelect(yearFromTimestamp(`reimbursement.timestamp`), "year")

      .addGroupBy(monthFromTimestamp(`reimbursement.timestamp`))
      .addGroupBy(yearFromTimestamp(`reimbursement.timestamp`));

    if (till) {
      qb.andWhere("reimbursement.timestamp <= :till", { till });
    }

    return (await qb.getRawMany()).map(monthlyReimbursement => {
      let entity = new CalculatedMonthlyReading();
      Object.assign(entity, monthlyReimbursement);
      return entity;
    });
  }

  @FieldResolver(() => [CaclulatedYearlyReading])
  protected async reimbursementByYear(
    @Root() { stationGroup }: StationGroupReadings,
    @Arg("rate") rate: number,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    if (rate <= 0) {
      throw new RangeError(`Rate should be greater than 0`);
    }
    let qb = this.manager
      .createQueryBuilder(EnergyReading, "reimbursement")
      .innerJoin("reimbursement.station", "station", "station.type = :type", {
        type: StationType.SUBSTATION
      })
      .innerJoin("station.group", "group")
      .where("reimbursement.timestamp >= :from AND group.id = :groupId", {
        from,
        groupId: stationGroup.id
      })
      .select(
        sqlConcatWithSeperator(
          `'/'`,
          wrapString(CalculatedReadingTypes.R_GROUPED_YEARLY),
          yearFromTimestamp(`reimbursement.timestamp`)
        ),
        "id"
      )
      .addSelect(toFixed(`SUM(reimbursement.value)*${rate}`), "value")
      .addSelect(yearFromTimestamp(`reimbursement.timestamp`), "year")
      .addGroupBy(yearFromTimestamp(`reimbursement.timestamp`));

    if (till) {
      qb.andWhere("reimbursement.timestamp <= :till", { till });
    }

    return (await qb.getRawMany()).map(yearlyReimbursement => {
      let entity = new CaclulatedYearlyReading();
      Object.assign(entity, yearlyReimbursement);
      return entity;
    });
  }
}
