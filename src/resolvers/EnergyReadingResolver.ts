import { GraphQLError } from "graphql";
import { DateTime } from "luxon";
import { Arg, ID, Mutation, Resolver } from "type-graphql";
import { EntityManager } from "typeorm";
import { InjectManager } from "typeorm-typedi-extensions";
import { EnergyReading } from "../entities/EnergyReading";
import { Station } from "../entities/Station";
import { Prefixes } from "../Prefixes";
import { AddEnergyReading } from "../types/AddEnergyReadingInput";
import { EnergyUnit } from "../types/EnergyUnit";

@Resolver(() => EnergyReading)
export class EnergyReadingResolver {
  @InjectManager() manager!: EntityManager;

  @Mutation(() => [EnergyReading])
  public async addEnergyReadings(
    @Arg("readings", () => [AddEnergyReading])
    addEnergyReadingInput: AddEnergyReading[],
    @Arg("stationUUID", () => ID, {
      description: "UUID of the station",
      nullable: true
    })
    stationUUID?: string,
    @Arg("stationId", () => ID, {
      description: "UUID of the station",
      nullable: true
    })
    stationId?: number
  ) {
    if (stationId === undefined && stationUUID === undefined) {
      throw new GraphQLError(`stationUUID or stationId should be defined`);
    }
    let station: Station | undefined;

    if (stationId) {
      station = await this.manager.findOne(Station, stationId);
    } else {
      station = await this.manager.findOne(Station, {
        uuid: stationUUID
      });
    }
    if (station === undefined) {
      throw new GraphQLError(`Station was not found`);
    }

    let readings = (<AddEnergyReading[]>[])
      .concat(addEnergyReadingInput)
      .map(reading => {
        let energyReading = new EnergyReading();

        let value = reading.value;
        switch (reading.unit) {
          case EnergyUnit.KWH:
            value = value * Prefixes.KILO;
        }

        energyReading.station = station!;
        energyReading.value = value;
        energyReading.timestamp = reading.timestamp;
        return energyReading;
      });

    await this.manager
      .createQueryBuilder()
      .insert()
      .into(EnergyReading)
      .onConflict(
        `(stationId, timestamp)
        DO UPDATE
        SET
           value = excluded.value,
           updatedDate = excluded.updatedDate
          `
      )
      .values(readings)
      .execute();

    return await this.manager
      .createQueryBuilder(EnergyReading, "energyReading")
      .where(
        "energyReading.stationId = :stationId AND energyReading.timestamp IN (:...amalgamation)",
        {
          amalgamation: readings.map(reading =>
            DateTime.fromJSDate(reading.timestamp)
              .toUTC()
              .toSQL({
                includeOffset: false
              })
          ),
          stationId: station.id
        }
      )
      .getMany();
  }
}
