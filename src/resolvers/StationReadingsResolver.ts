import { Arg, FieldResolver, Resolver, Root } from "type-graphql";
import { EntityManager } from "typeorm";
import { InjectManager } from "typeorm-typedi-extensions";
import { EnergyReading } from "../entities/EnergyReading";
import { PowerReading } from "../entities/PowerReading";
import { StationReadings } from "../StationReadings";
import { CalculatedMonthlyReading } from "../types/CalculatedMonthlyReading";
import { CaclulatedYearlyReading } from "../types/CalculatedYearlyReading";
import {
  sqlConcatWithSeperator,
  wrapString,
  yearFromTimestamp,
  paddedMonthFromTimestamp,
  dayFromTimestamp,
  monthFromTimestamp,
  toFixed
} from "../Utils";
import { CalculatedReadingTypes } from "../types/CalculatedReadingTypes";
import { CalculatedDailyReading } from "../types/CaclulatedDailyReading";

@Resolver(() => StationReadings)
export class StationReadingsResolver {
  @InjectManager() manager!: EntityManager;

  @FieldResolver(() => [PowerReading])
  protected async power(
    @Root() { station }: StationReadings,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    let qb = this.manager
      .createQueryBuilder(PowerReading, "powerReading")
      .where(
        "powerReading.timestamp >= :from AND powerReading.stationId = :stationId",
        { from, stationId: station.id }
      );

    if (till) {
      qb.andWhere("powerReading.timestamp <= :till", { till });
    }
    return await qb.getMany();
  }

  /**
   * Resolves daily energy readings for a Station
   * @param param0 A StationReadings type which is a wrapper type for a Station
   * @param from Lower date limit
   * @param till Upper date limit
   */
  @FieldResolver(() => [EnergyReading])
  protected async energy(
    @Root() { station }: StationReadings,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    let qb = this.manager
      .createQueryBuilder(EnergyReading, "energyReading")
      .where(
        "energyReading.timestamp >= :from AND energyReading.stationId = :stationId",
        { from, stationId: station.id }
      );

    if (till) {
      qb.andWhere("energyReading.timestamp <= :till", { till });
    }

    return await qb.getMany();
  }

  @FieldResolver(() => [CalculatedMonthlyReading])
  protected async energyByMonth(
    @Root() { station }: StationReadings,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    let qb = this.manager
      .createQueryBuilder(EnergyReading, "energyReading")
      .where(
        "energyReading.timestamp >= :from AND energyReading.stationId = :stationId",
        { from, stationId: station.id }
      )
      .select(
        sqlConcatWithSeperator(
          `'/'`,
          wrapString(CalculatedReadingTypes.E_MONTHLY),
          yearFromTimestamp(`energyReading.timestamp`),
          paddedMonthFromTimestamp(`energyReading.timestamp`)
        ),
        "id"
      )
      .addSelect(`SUM(energyReading.value)`, "value")
      .addSelect(monthFromTimestamp(`energyReading.timestamp`), "month")
      .addSelect(yearFromTimestamp(`energyReading.timestamp`), "year")
      .addGroupBy(monthFromTimestamp(`energyReading.timestamp`))
      .addGroupBy(yearFromTimestamp(`energyReading.timestamp`));

    if (till) {
      qb.andWhere("energyReading.timestamp <= :till", { till });
    }

    return (await qb.getRawMany()).map(monthlyEnergy => {
      let entity = new CalculatedMonthlyReading();
      Object.assign(entity, monthlyEnergy);
      return entity;
    });
  }

  @FieldResolver(() => [CaclulatedYearlyReading])
  protected async energyByYear(
    @Root() { station }: StationReadings,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    let qb = this.manager
      .createQueryBuilder(EnergyReading, "energyReading")
      .where(
        "energyReading.timestamp >= :from AND energyReading.stationId = :stationId",
        { from, stationId: station.id }
      )
      .select(
        sqlConcatWithSeperator(
          `'/'`,
          wrapString(CalculatedReadingTypes.E_YEARLY),
          yearFromTimestamp(`energyReading.timestamp`)
        ),
        "id"
      )
      .addSelect(`SUM(energyReading.value)`, "value")
      .addSelect(yearFromTimestamp(`energyReading.timestamp`), "year")

      .addGroupBy(yearFromTimestamp(`energyReading.timestamp`));

    if (till) {
      qb.andWhere("energyReading.timestamp <= :till", { till });
    }

    return (await qb.getRawMany()).map(yearlyEnergy => {
      let entity = new CaclulatedYearlyReading();
      Object.assign(entity, yearlyEnergy);
      return entity;
    });
  }

  @FieldResolver(() => [CalculatedDailyReading])
  protected async reimbursement(
    @Root() { station }: StationReadings,
    @Arg("rate") rate: number,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    if (rate <= 0) {
      throw new RangeError(`Rate should be greater than 0`);
    }

    let qb = this.manager
      .createQueryBuilder(EnergyReading, "reimbursement")
      .where(
        "reimbursement.timestamp >= :from AND reimbursement.stationId = :stationId",
        {
          from,
          stationId: station.id
        }
      );

    if (till) {
      qb.andWhere("reimbursement.timestamp <= :till", { till });
    }

    qb.select(
      sqlConcatWithSeperator(
        `'/'`,
        wrapString(CalculatedReadingTypes.R_DAILY),
        yearFromTimestamp(`reimbursement.timestamp`),
        paddedMonthFromTimestamp(`reimbursement.timestamp`),
        dayFromTimestamp(`reimbursement.timestamp`)
      ),
      "id"
    )
      .addSelect(toFixed(`SUM(reimbursement.value)*${rate}`), "value")

      .addSelect(dayFromTimestamp(`reimbursement.timestamp`), "day")
      .addSelect(monthFromTimestamp(`reimbursement.timestamp`), "month")
      .addSelect(yearFromTimestamp(`reimbursement.timestamp`), "year")

      .addGroupBy(dayFromTimestamp(`reimbursement.timestamp`))
      .addGroupBy(monthFromTimestamp(`reimbursement.timestamp`))
      .addGroupBy(yearFromTimestamp(`reimbursement.timestamp`));

    return (await qb.getRawMany()).map(dailyReimbursement => {
      let entity = new CalculatedDailyReading();
      Object.assign(entity, dailyReimbursement);
      return entity;
    });
  }
  @FieldResolver(() => [CalculatedMonthlyReading])
  protected async reimbursementByMonth(
    @Root() { station }: StationReadings,
    @Arg("rate") rate: number,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    if (rate <= 0) {
      throw new RangeError(`Rate should be greater than 0`);
    }

    let qb = this.manager
      .createQueryBuilder(EnergyReading, "reimbursement")
      .where(
        "reimbursement.timestamp >= :from AND reimbursement.stationId = :stationId",
        {
          from,
          stationId: station.id
        }
      );

    if (till) {
      qb.andWhere("reimbursement.timestamp <= :till", { till });
    }

    qb.select(
      sqlConcatWithSeperator(
        `'/'`,
        wrapString(CalculatedReadingTypes.R_MONTHLY),
        yearFromTimestamp(`reimbursement.timestamp`),
        paddedMonthFromTimestamp(`reimbursement.timestamp`)
      ),
      "id"
    )
      .addSelect(toFixed(`SUM(reimbursement.value)*${rate}`), "value")

      .addSelect(monthFromTimestamp(`reimbursement.timestamp`), "month")
      .addSelect(yearFromTimestamp(`reimbursement.timestamp`), "year")

      .addGroupBy(monthFromTimestamp(`reimbursement.timestamp`))
      .addGroupBy(yearFromTimestamp(`reimbursement.timestamp`));

    return (await qb.getRawMany()).map(monthlyReimbursement => {
      let entity = new CalculatedMonthlyReading();
      Object.assign(entity, monthlyReimbursement);
      return entity;
    });
  }

  @FieldResolver(() => [CaclulatedYearlyReading])
  protected async reimbursementByYear(
    @Root() { station }: StationReadings,
    @Arg("rate") rate: number,
    @Arg("from") from: Date,
    @Arg("till", {
      nullable: true
    })
    till?: Date
  ) {
    if (rate <= 0) {
      throw new RangeError(`Rate should be greater than 0`);
    }

    let qb = this.manager
      .createQueryBuilder(EnergyReading, "reimbursement")
      .where(
        "reimbursement.timestamp >= :from AND reimbursement.stationId = :stationId",
        {
          from,
          stationId: station.id
        }
      );

    if (till) {
      qb.andWhere("reimbursement.timestamp <= :till", { till });
    }

    qb.select(
      sqlConcatWithSeperator(
        `'/'`,
        wrapString(CalculatedReadingTypes.R_YEARLY),
        yearFromTimestamp(`reimbursement.timestamp`),
        paddedMonthFromTimestamp(`reimbursement.timestamp`),
        dayFromTimestamp(`reimbursement.timestamp`)
      ),
      "id"
    )
      .addSelect(toFixed(`SUM(reimbursement.value)*${rate}`), "value")

      .addSelect(dayFromTimestamp(`reimbursement.timestamp`), "day")
      .addSelect(monthFromTimestamp(`reimbursement.timestamp`), "month")
      .addSelect(yearFromTimestamp(`reimbursement.timestamp`), "year")

      .addGroupBy(dayFromTimestamp(`reimbursement.timestamp`))
      .addGroupBy(monthFromTimestamp(`reimbursement.timestamp`))
      .addGroupBy(yearFromTimestamp(`reimbursement.timestamp`));

    return (await qb.getRawMany()).map(yearlyReimbursement => {
      let entity = new CaclulatedYearlyReading();
      Object.assign(entity, yearlyReimbursement);
      return entity;
    });
  }
}
