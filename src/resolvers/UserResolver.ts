import bcrypt from "bcrypt";
import {
  Arg,
  Args,
  ArgsType,
  Field,
  FieldResolver,
  Int,
  Mutation,
  Query,
  Resolver,
  Root,
  Ctx,
  ForbiddenError
} from "type-graphql";
import { Repository } from "typeorm";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Role } from "../entities/Role";
import { User } from "../entities/User";
import { PaginationArgs } from "../types/Pagination";
import { RootContext } from "../types/Context";
import { AuthToken } from "../entities/AuthToken";

@ArgsType()
export class LoginInput implements Partial<User> {
  @Field()
  name!: string;
  @Field()
  password!: string;
}

@ArgsType()
export class AddUserInput implements Partial<User> {
  @Field()
  name!: string;

  @Field(type => Int)
  roleId!: number;

  @Field()
  password!: string;
}

@ArgsType()
export class DeleteUserInput implements Partial<User> {
  @Field(type => Int, { description: "Id to delete" })
  id!: number;
}

@ArgsType()
export class UpdateUserRole implements Partial<User> {
  @Field(type => Int)
  id!: number;

  @Field(type => Int)
  roleId!: number;
}

@Resolver(type => User)
export class UserResolver {
  @InjectRepository(User)
  private userRepo!: Repository<User>;

  @Query(type => User)
  user(
    @Arg("id", type => Int)
    id: number
  ) {
    return this.userRepo.findOneOrFail({
      where: {
        id: id
      }
    });
  }

  @Mutation(type => User)
  async login(
    @Args() { password, name }: LoginInput,
    @Ctx() { req, res, authorized }: RootContext
  ): Promise<AuthToken> {
    // Check if already logged in.
    // TODO: message
    if (authorized) throw new ForbiddenError();
    let user = await this.userRepo.findOne({ where: { name } });
    if (!user) throw new Error(`Username or password is incorrect`);
    let matches = await bcrypt.compare(password, user.hash);
    if (!matches) throw new Error(`Username or password is incorrect`);

    // Create a new AuthToken
    let token = new AuthToken();
    token.value = AuthToken.generateToken();
    token.user = user;
    token.touch();

    await this.userRepo.save(token);
    return token;
  }

  @Mutation(type => User)
  async createUser(@Args() { password, name, roleId }: AddUserInput): Promise<
    User
  > {
    let user = this.userRepo.create({
      name,
      hash: await bcrypt.hash(password, 10),
      role: {
        id: roleId
      }
    });

    return this.userRepo.save(user);
  }

  @Mutation(type => User)
  async deleteUser(@Args() { id }: DeleteUserInput): Promise<User> {
    let user = await this.userRepo.findOneOrFail(id);
    await this.userRepo.delete(id);
    return user;
  }

  @Mutation(type => User)
  async updateUserRole(@Args() { id }: UpdateUserRole): Promise<User> {
    let user = await this.userRepo.findOneOrFail(id);
    await this.userRepo.delete(id);
    return user;
  }

  @Query(type => [User])
  async users(@Args() { skip, take }: PaginationArgs): Promise<User[]> {
    return await this.userRepo.find({
      skip,
      take
    });
  }

  @FieldResolver(type => Role)
  async role(@Root() user: User): Promise<Role> {
    let role = (await this.userRepo
      .createQueryBuilder()
      .relation(User, "role")
      .of(user)
      .loadOne()) as Role;

    return role;
  }
}
