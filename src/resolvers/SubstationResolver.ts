import { Arg, Mutation, Resolver, FieldResolver, Root, Ctx } from "type-graphql";
import { StationGroup } from "../entities/StationGroup";
import { Substation } from "../entities/Substation";
import { EntityDoesNotExistError } from "../errors/EntityDoesNotExistError";
import { StationResolver } from "./StationResolver";
import { AddSubstationInput } from "../types/AddSubstationInput";
import { PowerReading } from "../entities/PowerReading";
import { Station } from "../entities/Station";
import { StationReadings } from "../StationReadings";
import { BasicStation } from "../entities/BasicStation";
import { RootContext } from "../types/Context";

@Resolver(()=> Substation)
export class SubstationResolver extends StationResolver {
  @Mutation(() => Substation)
  public async addSubstation(@Arg("input") input: AddSubstationInput) {
    await this.assertGroup(input.groupId);

    let substation: Substation = await this.addStation(Substation, input) as Substation;
    substation.group = { id: input.groupId } as StationGroup;
    await this.manager.insert(Substation, substation);
    return substation;
  }

  /**
   * Helper to assert the existence of a group
   * @param id An id of a group
   */
  private async assertGroup(id: number) {
    if (!(await this.manager.findOne(StationGroup, id))) {
      throw new EntityDoesNotExistError(StationGroup, id);
    }
  }
  
  @FieldResolver(() => StationReadings)
  public readings(@Root() station: Substation, @Ctx() ctx: RootContext) {
    return this.getReadings(station);
  }
}


