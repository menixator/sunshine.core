import { Resolver, Mutation, Arg, ID } from "type-graphql";
import { PowerReading } from "../entities/PowerReading";
import { GraphQLError } from "graphql";
import { AddPowerReadingInput } from "../types/AddPowerReadingInput";
import { Station } from "../entities/Station";
import { InjectManager } from "typeorm-typedi-extensions";
import { EntityManager } from "typeorm";
import { PowerUnit } from "../types/PowerUnit";
import { Prefixes } from "../Prefixes";
import { DateTime } from "luxon";

@Resolver(() => PowerReading)
export class PowerReadingResolver {
  @InjectManager() manager!: EntityManager;

  @Mutation(() => [PowerReading])
  public async addPowerReadings(
    @Arg("readings", () => [AddPowerReadingInput])
    addPowerReadingInput: AddPowerReadingInput[],
    @Arg("stationUUID", () => ID, {
      description: "UUID of the station",
      nullable: true
    })
    stationUUID?: string,
    @Arg("stationId", () => ID, {
      description: "UUID of the station",
      nullable: true
    })
    stationId?: number
  ) {
    if (stationId === undefined && stationUUID === undefined) {
      throw new GraphQLError(`stationUUID or stationId should be defined`);
    }
    let station: Station | undefined;

    if (stationId) {
      station = await this.manager.findOne(Station, stationId);
    } else {
      station = await this.manager.findOne(Station, {
        uuid: stationUUID
      });
    }
    if (station === undefined) {
      throw new GraphQLError(`Station was not found`);
    }

    let readings = (<AddPowerReadingInput[]>[])
      .concat(addPowerReadingInput)
      .map(reading => {
        let powerReading = new PowerReading();

        let value = reading.value;
        switch (reading.unit) {
          case PowerUnit.KW:
            value = value * Prefixes.KILO;
        }

        powerReading.station = station!;
        powerReading.value = value;
        powerReading.timestamp = reading.timestamp;
        return powerReading;
      });

    await this.manager
      .createQueryBuilder()
      .insert()
      .into(PowerReading)
      .onConflict(
        `(stationId, timestamp)
        DO UPDATE
        SET
           value = excluded.value,
           updatedDate = excluded.updatedDate
          `
      )
      .values(readings)
      .execute();

    return await this.manager
      .createQueryBuilder(PowerReading, "powerReading")
      .where(
        "powerReading.stationId = :stationId AND powerReading.timestamp IN (:...amalgamation)",
        {
          amalgamation: readings.map(reading =>
            DateTime.fromJSDate(reading.timestamp)
              .toUTC()
              .toSQL({
                includeOffset: false
              })
          ),
          stationId: station.id
        }
      )
      .getMany();
  }
}
