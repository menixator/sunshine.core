import {
  Arg,
  Mutation,
  Resolver,
  FieldResolver,
  Root,
  Ctx
} from "type-graphql";
import { BasicStation } from "../entities/BasicStation";
import { AddBasicStationInput } from "../types/AddBasicStationInput";
import { StationResolver } from "./StationResolver";
import { PowerReading } from "../entities/PowerReading";
import { Station } from "../entities/Station";
import { StationReadings } from "../StationReadings";
import { RootContext } from "../types/Context";

@Resolver(() => BasicStation)
export class BasicStationResolver extends StationResolver {
  @Mutation(() => BasicStation)
  public async addBasicStation(@Arg("input") input: AddBasicStationInput) {
    let basicStation = await this.addStation(BasicStation, input);
    await this.manager.insert(BasicStation, basicStation);
    return basicStation;
  }

  @FieldResolver(() => StationReadings)
  public readings(@Root() station: BasicStation, @Ctx() ctx: RootContext) {
    return this.getReadings(station);
  }
}
