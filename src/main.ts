import "reflect-metadata";
import { GraphQLOptions } from "apollo-server-core";
import { graphiqlExpress, graphqlExpress } from "apollo-server-express";
import cp from "cookie-parser";
import express from "express";
import { join } from "path";
import { buildSchema, useContainer as useGraphQLContainer } from "type-graphql";
import { Container } from "typedi";
import { createConnection, getManager, useContainer as useTypeORMContainer, getConnectionOptions } from "typeorm";
import { AuthToken, COOKIE_NAME } from "./entities/AuthToken";
import { RootContext } from "./types/Context";
import { AdvancedPrettyLogger } from "./AdvancedPrettyLogger";

useTypeORMContainer(Container);
useGraphQLContainer(Container);

export const app = express();

app.use(cp());

app.use("/api/v1", async (req, res, next) => {
  let cookie: string | null = req.cookies[COOKIE_NAME] || null;
  res.locals.context = <Partial<RootContext>>{
    authorized: false
  };

  if (cookie === null) {
  }
  let manager = await getManager();
  let token = await manager.findOne(AuthToken, {
    where: {
      value: cookie
    },
    relations: ["user", "user.role"]
  });

  if (!token || token.expired) {
    res.clearCookie(COOKIE_NAME);
  } else {
    token.touch();
    await manager.save(token);
    res.locals.context.user = token.user;
    res.locals.context.token = token;
    res.locals.context.role = token.user.role;
    res.locals.context.authorized = true;
  }
  next();
});

async function main() {
  let connectionOptions = await getConnectionOptions();
  //@ts-ignore
  connectionOptions.logger = new AdvancedPrettyLogger(true);
  await createConnection(connectionOptions);

  const schema = await buildSchema({
    resolvers: [join(__dirname, "./resolvers/**/*")]
  });

  app.use(
    "/api/v1",
    express.json(),
    graphqlExpress(
      (req, res): GraphQLOptions<RootContext> => {
        return {
          schema,
          context: Object.assign(
            { req, res },
            res!.locals.context
          ) as RootContext
        } as GraphQLOptions<RootContext>;
      }
    )
  );

  app.listen(3001);
  console.log(`started listening at http://localhost:${3001}`);
}

app.use(
  "/graphiql",
  graphiqlExpress({
    endpointURL: "/api/v1"
  })
);

main().catch(err => {
  console.error(err);
});
