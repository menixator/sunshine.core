import { GraphQLError, ASTNode, Source } from "graphql";
import { ObjectType } from "typeorm/common/ObjectType";
import Maybe from "graphql/tsutils/Maybe";
import { getConnection } from "typeorm";

/**
 * A helper to create errors when entities do not exist.
 */
export class EntityDoesNotExistError<T> extends GraphQLError {
  constructor(
    entity: ObjectType<T>,
    id: number,
    nodes?: ReadonlyArray<ASTNode> | ASTNode | undefined,
    source?: Maybe<Source>,
    positions?: Maybe<ReadonlyArray<number>>,
    path?: Maybe<ReadonlyArray<string | number>>,
    originalError?: Maybe<Error>,
    extensions?: Maybe<{ [key: string]: any }>
  ) {
    super(
      `${getConnection().getMetadata(entity).name} with id: '${id}' does not exist`,
      nodes,
      source,
      positions,
      path,
      originalError,
      extensions
    );
  }
}
