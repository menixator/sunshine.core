import { ASTNode, GraphQLError, Source } from "graphql";
import Maybe from "graphql/tsutils/Maybe";

/**
 * A generic constraint failed error
 */
export class ConstraintError extends GraphQLError {
  constructor(
    message: string,
    nodes?: ReadonlyArray<ASTNode> | ASTNode | undefined,
    source?: Maybe<Source>,
    positions?: Maybe<ReadonlyArray<number>>,
    path?: Maybe<ReadonlyArray<string | number>>,
    originalError?: Maybe<Error>,
    extensions?: Maybe<{ [key: string]: any }>
  ) {
    super(message, nodes, source, positions, path, originalError, extensions);
  }
}
