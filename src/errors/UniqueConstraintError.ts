import { ASTNode, Source } from "graphql";
import Maybe from "graphql/tsutils/Maybe";
import { ObjectType, getConnection } from "typeorm";
import { ConstraintError } from "./ConstraintError";

/**
 * A helper to create unique constraint errors
 */
export class UniqueConstraintError<T> extends ConstraintError {
  constructor(
    entity: ObjectType<T>,
    columnName: string,
    value: any,
    nodes?: ReadonlyArray<ASTNode> | ASTNode | undefined,
    source?: Maybe<Source>,
    positions?: Maybe<ReadonlyArray<number>>,
    path?: Maybe<ReadonlyArray<string | number>>,
    originalError?: Maybe<Error>,
    extensions?: Maybe<{ [key: string]: any }>
  ) {
    super(
      `There already exists an entity of type ${
      getConnection().getMetadata(entity).name
      } with the '${columnName}' : '${value}'`,
      nodes,
      source,
      positions,
      path,
      originalError,
      extensions
    );
  }
}
