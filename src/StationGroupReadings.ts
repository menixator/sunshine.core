import { Station } from "./entities/Station";
import { ObjectType } from "type-graphql";
import { StationGroup } from "./entities/StationGroup";

@ObjectType()
export class StationGroupReadings {
  public stationGroup!: StationGroup;


}
