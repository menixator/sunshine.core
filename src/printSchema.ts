import { join } from "path";
import { buildSchema } from "type-graphql";
import { printSchema } from "graphql";

async function main() {
  const schema = await buildSchema({
    resolvers: [join(__dirname, "./resolvers/**/*")]
  });

  console.log(printSchema(schema))
}


main().catch(err => {
    console.error(err);
    process.exit(-1);
})