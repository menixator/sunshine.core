export enum Prefixes {
  KILO = 10 ** 3,
  MEGA = 10 ** 6,
  GIGA = 10 ** 9
}
