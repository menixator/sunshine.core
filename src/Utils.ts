export function yearFromTimestamp(columnName: string) {
  return `strftime("%Y", ${columnName})`;
}

export function paddedMonthFromTimestamp(columnName: string) {
  return `substr('0' || (strftime("%m", ${columnName})-1), -2)`;
}

export function monthFromTimestamp(columnName: string) {
  return `strftime("%m", ${columnName})-1`;
}

export function dayFromTimestamp(columnName: string) {
  return `strftime("%d", ${columnName})`;
}

export function sqlConcat(...expressions: string[]) {
  return `(${expressions.join(" || ")})`;
}

export function sqlConcatWithSeperator(
  seperator: string,
  ...expressions: string[]
) {
  for (let i = expressions.length - 1; i > 0; i--) {
    expressions.splice(i, 0, `${seperator}`);
  }
  return sqlConcat(...expressions);
}

export function wrapString(string: string) {
  return `'${string}'`;
}

export function toFixed(expression: string, decimalPlaces: number = 2) {
  return `printf("%.${decimalPlaces}f", ${expression})`;
}
